-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2017 at 10:13 PM
-- Server version: 5.5.58-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `Reason` varchar(128) NOT NULL,
  `BannedBy` varchar(30) NOT NULL,
  `Date` varchar(128) NOT NULL,
  `IP` varchar(30) NOT NULL,
  `HWID` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE IF NOT EXISTS `businesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `OwnerSQL` int(11) NOT NULL DEFAULT '0',
  `InteriorX` float NOT NULL,
  `InteriorY` float NOT NULL,
  `InteriorZ` float NOT NULL,
  `InteriorA` float NOT NULL,
  `World` int(11) NOT NULL,
  `InteriorID` int(11) NOT NULL,
  `ExteriorX` float NOT NULL,
  `ExteriorY` float NOT NULL,
  `ExteriorZ` float NOT NULL,
  `BankPX` float NOT NULL,
  `BankPY` float NOT NULL,
  `BankPZ` float NOT NULL,
  `BankPIntID` int(11) NOT NULL,
  `BankPWorld` int(11) NOT NULL,
  `Name` varchar(128) NOT NULL,
  `Type` int(11) NOT NULL,
  `MarketPrice` int(11) NOT NULL,
  `Locked` int(11) NOT NULL DEFAULT '0',
  `Fee` int(11) NOT NULL,
  `Cashbox` int(11) NOT NULL DEFAULT '0',
  `Level` int(11) NOT NULL DEFAULT '1',
  `Product` int(11) NOT NULL DEFAULT '0',
  `Weapons0` int(11) NOT NULL,
  `Weapons1` int(11) NOT NULL,
  `Weapons2` int(11) NOT NULL,
  `Weapons3` int(11) NOT NULL,
  `Weapons4` int(11) NOT NULL,
  `Weapons5` int(11) NOT NULL,
  `Weapons6` int(11) NOT NULL,
  `Weapons7` int(11) NOT NULL,
  `Weapons8` int(11) NOT NULL,
  `Weapons9` int(11) NOT NULL,
  `Weapons10` int(11) NOT NULL,
  `Weapons11` int(11) NOT NULL,
  `Weapons12` int(11) NOT NULL,
  `Weapons13` int(11) NOT NULL,
  `Weapons14` int(11) NOT NULL,
  `Weapons15` int(11) NOT NULL,
  `Weapons16` int(11) NOT NULL,
  `Weapons17` int(11) NOT NULL,
  `Weapons18` int(11) NOT NULL,
  `Weapons19` int(11) NOT NULL,
  `Weapons20` int(11) NOT NULL,
  `Ammo0` int(11) NOT NULL,
  `Ammo1` int(11) NOT NULL,
  `Ammo2` int(11) NOT NULL,
  `Ammo3` int(11) NOT NULL,
  `Ammo4` int(11) NOT NULL,
  `Ammo5` int(11) NOT NULL,
  `Ammo6` int(11) NOT NULL,
  `Ammo7` int(11) NOT NULL,
  `Ammo8` int(11) NOT NULL,
  `Ammo9` int(11) NOT NULL,
  `Ammo10` int(11) NOT NULL,
  `Ammo11` int(11) NOT NULL,
  `Ammo12` int(11) NOT NULL,
  `Ammo13` int(11) NOT NULL,
  `Ammo14` int(11) NOT NULL,
  `Ammo15` int(11) NOT NULL,
  `Ammo16` int(11) NOT NULL,
  `Ammo17` int(11) NOT NULL,
  `Ammo18` int(11) NOT NULL,
  `Ammo19` int(11) NOT NULL,
  `Ammo20` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `OwnerSQL`, `InteriorX`, `InteriorY`, `InteriorZ`, `InteriorA`, `World`, `InteriorID`, `ExteriorX`, `ExteriorY`, `ExteriorZ`, `BankPX`, `BankPY`, `BankPZ`, `BankPIntID`, `BankPWorld`, `Name`, `Type`, `MarketPrice`, `Locked`, `Fee`, `Cashbox`, `Level`, `Product`, `Weapons0`, `Weapons1`, `Weapons2`, `Weapons3`, `Weapons4`, `Weapons5`, `Weapons6`, `Weapons7`, `Weapons8`, `Weapons9`, `Weapons10`, `Weapons11`, `Weapons12`, `Weapons13`, `Weapons14`, `Weapons15`, `Weapons16`, `Weapons17`, `Weapons18`, `Weapons19`, `Weapons20`, `Ammo0`, `Ammo1`, `Ammo2`, `Ammo3`, `Ammo4`, `Ammo5`, `Ammo6`, `Ammo7`, `Ammo8`, `Ammo9`, `Ammo10`, `Ammo11`, `Ammo12`, `Ammo13`, `Ammo14`, `Ammo15`, `Ammo16`, `Ammo17`, `Ammo18`, `Ammo19`, `Ammo20`) VALUES
(48, 0, 1704.18, -1489.77, 13.389, 86.951, 28163, 0, 1702.96, -1471.35, 13.547, 0, 0, 0, 0, 0, 'ffff', 9, 999, 0, 1, 0, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `criminal_record`
--

CREATE TABLE IF NOT EXISTS `criminal_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_name` varchar(32) NOT NULL,
  `entry_reason` varchar(128) NOT NULL,
  `entry_date` varchar(90) NOT NULL,
  `entry_by` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `doors`
--

CREATE TABLE IF NOT EXISTS `doors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `PosInterior` int(11) NOT NULL DEFAULT '0',
  `PosWorld` int(11) NOT NULL DEFAULT '0',
  `IntX` float NOT NULL,
  `IntY` float NOT NULL,
  `IntZ` float NOT NULL,
  `IntA` float NOT NULL,
  `IntInterior` int(11) NOT NULL DEFAULT '0',
  `IntWorld` int(11) NOT NULL DEFAULT '0',
  `Faction` int(11) NOT NULL DEFAULT '0',
  `Name` varchar(128) NOT NULL DEFAULT 'None',
  `Locked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entrances`
--

CREATE TABLE IF NOT EXISTS `entrances` (
  `InteriorDBID` int(11) NOT NULL AUTO_INCREMENT,
  `EntranceX` float NOT NULL,
  `EntranceY` float NOT NULL,
  `EntranceZ` float NOT NULL,
  `EntranceInteriorID` int(11) NOT NULL DEFAULT '0',
  `EntranceWorld` int(11) NOT NULL DEFAULT '0',
  `ExitX` float NOT NULL,
  `ExitY` float NOT NULL,
  `ExitZ` float NOT NULL,
  `ExitInteriorID` int(11) NOT NULL,
  `ExitWorld` int(11) NOT NULL,
  `FactionOwner` int(11) NOT NULL DEFAULT '0',
  `IsBlackMarket` int(11) NOT NULL DEFAULT '0',
  `IsWarehouse` int(11) NOT NULL DEFAULT '0',
  `Locked` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`InteriorDBID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `factioncars`
--

CREATE TABLE IF NOT EXISTS `factioncars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factionid` int(11) NOT NULL,
  `ModelID` int(11) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `PosA` float NOT NULL,
  `Interior` int(11) NOT NULL DEFAULT '0',
  `World` int(11) NOT NULL DEFAULT '0',
  `Color1` int(11) NOT NULL,
  `Color2` int(11) NOT NULL,
  `Sirens` int(11) NOT NULL DEFAULT '0',
  `Gunrack` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=147 ;

--
-- Dumping data for table `factioncars`
--

INSERT INTO `factioncars` (`id`, `factionid`, `ModelID`, `PosX`, `PosY`, `PosZ`, `PosA`, `Interior`, `World`, `Color1`, `Color2`, `Sirens`, `Gunrack`) VALUES
(144, 1, 596, 1528.34, -1683.19, 5.643, 275.706, 0, 0, 27, 131, 0, 1),
(145, 1, 596, 1617.62, -2313.08, 13.248, 244.767, 0, 0, 235, 140, 0, 0),
(146, 1, 596, 1533.28, -1690.61, 13.102, 181.385, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `factions`
--

CREATE TABLE IF NOT EXISTS `factions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(128) NOT NULL,
  `Abbreviation` varchar(128) NOT NULL,
  `MaxRanks` int(11) NOT NULL DEFAULT '20',
  `EditRank` int(11) NOT NULL DEFAULT '1',
  `ChatRank` int(11) NOT NULL DEFAULT '20',
  `TowRank` int(11) NOT NULL DEFAULT '1',
  `ChatColor` int(11) NOT NULL DEFAULT '-1920073814',
  `ChatStatus` int(11) NOT NULL DEFAULT '0',
  `CopPerms` int(11) NOT NULL DEFAULT '0',
  `MedPerms` int(11) NOT NULL DEFAULT '0',
  `SpawnX` float NOT NULL,
  `SpawnY` float NOT NULL,
  `SpawnZ` float NOT NULL,
  `SpawnA` float NOT NULL,
  `ExSpawn1X` float NOT NULL,
  `ExSpawn1Y` float NOT NULL,
  `ExSpawn1Z` float NOT NULL,
  `ExSpawn2X` float NOT NULL,
  `ExSpawn2Y` float NOT NULL,
  `ExSpawn2Z` float NOT NULL,
  `ExSpawn3X` float NOT NULL,
  `ExSpawn3Y` float NOT NULL,
  `ExSpawn3Z` float NOT NULL,
  `Bank` int(11) NOT NULL DEFAULT '0',
  `HasWarehouseAccess` int(11) NOT NULL DEFAULT '0',
  `Products` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `factions`
--

INSERT INTO `factions` (`id`, `Name`, `Abbreviation`, `MaxRanks`, `EditRank`, `ChatRank`, `TowRank`, `ChatColor`, `ChatStatus`, `CopPerms`, `MedPerms`, `SpawnX`, `SpawnY`, `SpawnZ`, `SpawnA`, `ExSpawn1X`, `ExSpawn1Y`, `ExSpawn1Z`, `ExSpawn2X`, `ExSpawn2Y`, `ExSpawn2Z`, `ExSpawn3X`, `ExSpawn3Y`, `ExSpawn3Z`, `Bank`, `HasWarehouseAccess`, `Products`) VALUES
(8, 'LSPD', 'ff', 20, 1, 20, 1, -1920073814, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `faction_ranks`
--

CREATE TABLE IF NOT EXISTS `faction_ranks` (
  `faction_id` int(11) NOT NULL,
  `factionrank1` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank2` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank3` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank4` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank5` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank6` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank7` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank8` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank9` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank10` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank11` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank12` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank13` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank14` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank15` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank16` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank17` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank18` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank19` varchar(60) NOT NULL DEFAULT 'NotSet',
  `factionrank20` varchar(60) NOT NULL DEFAULT 'NotSet',
  PRIMARY KEY (`faction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `furniture`
--

CREATE TABLE IF NOT EXISTS `furniture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `propertyid` int(11) NOT NULL,
  `model` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `RotX` float NOT NULL,
  `RotY` float NOT NULL,
  `RotZ` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1795 ;

--
-- Dumping data for table `furniture`
--

INSERT INTO `furniture` (`id`, `propertyid`, `model`, `name`, `PosX`, `PosY`, `PosZ`, `RotX`, `RotY`, `RotZ`) VALUES
(1792, 610, 19355, 'Wall 1', -2171.7, 643.187, 1053.38, 0, 0, 0),
(1793, 610, 19462, 'Floor 1 (Big)', -2169.17, 643.439, 1053.38, 0, 0, 0),
(1794, 610, 1700, 'Bed 1', -2169.73, 643.652, 1053.38, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `log_ajail`
--

CREATE TABLE IF NOT EXISTS `log_ajail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) NOT NULL,
  `Reason` varchar(128) NOT NULL,
  `JailedBy` varchar(40) NOT NULL,
  `Date` varchar(128) NOT NULL,
  `Time` int(11) NOT NULL,
  `IP` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `log_ajail`
--

INSERT INTO `log_ajail` (`id`, `Name`, `Reason`, `JailedBy`, `Date`, `Time`, `IP`) VALUES
(1, 'Samuel_Morris', 'Test', 'Samuel_Morris', '2017-10-24 22:45:45', 1, '151.231.230.165');

-- --------------------------------------------------------

--
-- Table structure for table `log_anotes`
--

CREATE TABLE IF NOT EXISTS `log_anotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) NOT NULL,
  `Reason` varchar(256) NOT NULL,
  `Date` varchar(128) NOT NULL,
  `IP` varchar(40) NOT NULL,
  `AddedBy` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_bans`
--

CREATE TABLE IF NOT EXISTS `log_bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(34) NOT NULL,
  `Reason` varchar(128) NOT NULL,
  `BannedBy` varchar(34) NOT NULL,
  `Date` varchar(90) NOT NULL,
  `IP` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_kicks`
--

CREATE TABLE IF NOT EXISTS `log_kicks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) NOT NULL,
  `KickedBy` varchar(40) NOT NULL,
  `Reason` varchar(128) NOT NULL,
  `Date` varchar(128) NOT NULL,
  `IP` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `log_kicks`
--

INSERT INTO `log_kicks` (`id`, `Name`, `KickedBy`, `Reason`, `Date`, `IP`) VALUES
(39, 'Ryan_Everett', 'Ryan_Everett', 'i do the sleeps :x', '2017-10-20 06:25:03', '174.59.17.224');

-- --------------------------------------------------------

--
-- Table structure for table `log_passchanges`
--

CREATE TABLE IF NOT EXISTS `log_passchanges` (
  `player_name` varchar(60) NOT NULL,
  `account_dbid` int(11) NOT NULL,
  `change_date` varchar(90) NOT NULL,
  `player_ip` varchar(60) NOT NULL,
  `player_hwid` varchar(90) NOT NULL,
  `old_password` varchar(128) NOT NULL,
  `new_password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `RotX` float NOT NULL,
  `RotY` float NOT NULL,
  `RotZ` float NOT NULL,
  `Interior` int(11) NOT NULL,
  `World` int(11) NOT NULL,
  `OpenX` float NOT NULL,
  `OpenY` float NOT NULL,
  `OpenZ` float NOT NULL,
  `OpenRotX` float NOT NULL,
  `OpenRotY` float NOT NULL,
  `OpenRotZ` float NOT NULL,
  `Type` int(11) NOT NULL DEFAULT '0',
  `Faction` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ownedcars`
--

CREATE TABLE IF NOT EXISTS `ownedcars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `OwnerID` int(11) NOT NULL,
  `ModelID` int(11) NOT NULL,
  `PosX` float NOT NULL DEFAULT '1708.04',
  `PosY` float NOT NULL DEFAULT '-1035.8',
  `PosZ` float NOT NULL DEFAULT '23.9063',
  `PosA` float NOT NULL DEFAULT '358.535',
  `Interior` int(11) NOT NULL,
  `World` int(11) NOT NULL,
  `Color1` int(11) NOT NULL,
  `Color2` int(11) NOT NULL,
  `Locked` int(11) NOT NULL,
  `Fuel` float NOT NULL DEFAULT '100',
  `Paintjob` int(11) NOT NULL DEFAULT '-1',
  `XMR` int(11) NOT NULL DEFAULT '0',
  `Impounded` int(11) NOT NULL DEFAULT '0',
  `ImpoundPosX` float NOT NULL,
  `ImpoundPosY` float NOT NULL,
  `ImpoundPosZ` float NOT NULL,
  `ImpoundPosA` float NOT NULL,
  `EngineLife` float NOT NULL DEFAULT '100',
  `BatteryLife` float NOT NULL DEFAULT '100',
  `LockLevel` int(11) NOT NULL DEFAULT '1',
  `AlarmLevel` int(11) NOT NULL DEFAULT '1',
  `ImmobLevel` int(11) NOT NULL DEFAULT '1',
  `Insurance` int(11) NOT NULL DEFAULT '0',
  `TimesDestroyed` int(11) NOT NULL DEFAULT '0',
  `Plate` varchar(32) NOT NULL,
  `ReportedStolen` tinyint(1) NOT NULL DEFAULT '0',
  `ReportedStolenDate` varchar(90) NOT NULL,
  `CarMods1` int(11) NOT NULL DEFAULT '-1',
  `CarMods2` int(11) NOT NULL DEFAULT '-1',
  `CarMods3` int(11) NOT NULL DEFAULT '-1',
  `CarMods4` int(11) NOT NULL DEFAULT '-1',
  `CarMods5` int(11) NOT NULL DEFAULT '-1',
  `CarMods6` int(11) NOT NULL DEFAULT '-1',
  `CarMods7` int(11) NOT NULL DEFAULT '-1',
  `CarMods8` int(11) NOT NULL DEFAULT '-1',
  `CarMods9` int(11) NOT NULL DEFAULT '-1',
  `CarMods10` int(11) NOT NULL DEFAULT '-1',
  `CarMods11` int(11) NOT NULL DEFAULT '-1',
  `CarMods12` int(11) NOT NULL DEFAULT '-1',
  `CarMods13` int(11) NOT NULL DEFAULT '-1',
  `CarMods14` int(11) NOT NULL DEFAULT '-1',
  `Weapons1` int(11) NOT NULL,
  `Weapons2` int(11) NOT NULL,
  `Weapons3` int(11) NOT NULL,
  `Weapons4` int(11) NOT NULL,
  `Weapons5` int(11) NOT NULL,
  `Ammo1` int(11) NOT NULL,
  `Ammo2` int(11) NOT NULL,
  `Ammo3` int(11) NOT NULL,
  `Ammo4` int(11) NOT NULL,
  `Ammo5` int(11) NOT NULL,
  `Packages1` int(11) NOT NULL,
  `Packages2` int(11) NOT NULL,
  `Packages3` int(11) NOT NULL,
  `Packages4` int(11) NOT NULL,
  `Packages5` int(11) NOT NULL,
  `Packages6` int(11) NOT NULL,
  `Packages7` int(11) NOT NULL,
  `Packages8` int(11) NOT NULL,
  `Packages9` int(11) NOT NULL,
  `Packages10` int(11) NOT NULL,
  `Packages11` int(11) NOT NULL,
  `Packages12` int(11) NOT NULL,
  `Packages13` int(11) NOT NULL,
  `Packages14` int(11) NOT NULL,
  `Packages15` int(11) NOT NULL,
  `Packages16` int(11) NOT NULL,
  `Packages17` int(11) NOT NULL,
  `Packages18` int(11) NOT NULL,
  `Packages19` int(11) NOT NULL,
  `Packages20` int(11) NOT NULL,
  `PackagesAmmo1` int(11) NOT NULL,
  `PackagesAmmo2` int(11) NOT NULL,
  `PackagesAmmo3` int(11) NOT NULL,
  `PackagesAmmo4` int(11) NOT NULL,
  `PackagesAmmo5` int(11) NOT NULL,
  `PackagesAmmo6` int(11) NOT NULL,
  `PackagesAmmo7` int(11) NOT NULL,
  `PackagesAmmo8` int(11) NOT NULL,
  `PackagesAmmo9` int(11) NOT NULL,
  `PackagesAmmo10` int(11) NOT NULL,
  `PackagesAmmo11` int(11) NOT NULL,
  `PackagesAmmo12` int(11) NOT NULL,
  `PackagesAmmo13` int(11) NOT NULL,
  `PackagesAmmo14` int(11) NOT NULL,
  `PackagesAmmo15` int(11) NOT NULL,
  `PackagesAmmo16` int(11) NOT NULL,
  `PackagesAmmo17` int(11) NOT NULL,
  `PackagesAmmo18` int(11) NOT NULL,
  `PackagesAmmo19` int(11) NOT NULL,
  `PackagesAmmo20` int(11) NOT NULL,
  `LastDriver` int(11) NOT NULL,
  `LastPassenger` int(11) NOT NULL,
  `drugType1` int(11) NOT NULL,
  `drugType2` int(11) NOT NULL,
  `drugType3` int(11) NOT NULL,
  `drugType4` int(11) NOT NULL,
  `drugType5` int(11) NOT NULL,
  `drugType6` int(11) NOT NULL,
  `drugType7` int(11) NOT NULL,
  `drugType8` int(11) NOT NULL,
  `drugType9` int(11) NOT NULL,
  `drugType10` int(11) NOT NULL,
  `drugType11` int(11) NOT NULL,
  `drugType12` int(11) NOT NULL,
  `drugType13` int(11) NOT NULL,
  `drugType14` int(11) NOT NULL,
  `drugType15` int(11) NOT NULL,
  `drugType16` int(11) NOT NULL,
  `drugType17` int(11) NOT NULL,
  `drugType18` int(11) NOT NULL,
  `drugType19` int(11) NOT NULL,
  `drugType20` int(11) NOT NULL,
  `drugQuantity1` int(11) NOT NULL,
  `drugQuantity2` int(11) NOT NULL,
  `drugQuantity3` int(11) NOT NULL,
  `drugQuantity4` int(11) NOT NULL,
  `drugQuantity5` int(11) NOT NULL,
  `drugQuantity6` int(11) NOT NULL,
  `drugQuantity7` int(11) NOT NULL,
  `drugQuantity8` int(11) NOT NULL,
  `drugQuantity9` int(11) NOT NULL,
  `drugQuantity10` int(11) NOT NULL,
  `drugQuantity11` int(11) NOT NULL,
  `drugQuantity12` int(11) NOT NULL,
  `drugQuantity13` int(11) NOT NULL,
  `drugQuantity14` int(11) NOT NULL,
  `drugQuantity15` int(11) NOT NULL,
  `drugQuantity16` int(11) NOT NULL,
  `drugQuantity17` int(11) NOT NULL,
  `drugQuantity18` int(11) NOT NULL,
  `drugQuantity19` int(11) NOT NULL,
  `drugQuantity20` int(11) NOT NULL,
  `drugType21` int(11) NOT NULL,
  `drugType22` int(11) NOT NULL,
  `drugType23` int(11) NOT NULL,
  `drugType24` int(11) NOT NULL,
  `drugQuantity21` int(11) NOT NULL,
  `drugQuantity22` int(11) NOT NULL,
  `drugQuantity23` int(11) NOT NULL,
  `drugQuantity24` int(11) NOT NULL,
  `Weapon1PosX` float NOT NULL,
  `Weapon1PosY` float NOT NULL,
  `Weapon1PosZ` float NOT NULL DEFAULT '0.0000009537',
  `Weapon1RotX` float NOT NULL,
  `Weapon1RotY` float NOT NULL,
  `Weapon1RotZ` float NOT NULL,
  `Weapon2PosX` float NOT NULL,
  `Weapon2PosY` float NOT NULL,
  `Weapon2PosZ` float NOT NULL DEFAULT '0.0000009537',
  `Weapon2RotX` float NOT NULL,
  `Weapon2RotY` float NOT NULL,
  `Weapon2RotZ` float NOT NULL,
  `Weapon3PosX` float NOT NULL,
  `Weapon3PosY` float NOT NULL,
  `Weapon3PosZ` float NOT NULL DEFAULT '0.0000009537',
  `Weapon3RotX` float NOT NULL,
  `Weapon3RotY` float NOT NULL,
  `Weapon3RotZ` float NOT NULL,
  `Weapon4PosX` float NOT NULL,
  `Weapon4PosY` float NOT NULL,
  `Weapon4PosZ` float NOT NULL DEFAULT '0.0000009537',
  `Weapon4RotX` float NOT NULL,
  `Weapon4RotY` float NOT NULL,
  `Weapon4RotZ` float NOT NULL,
  `LastHealth` float NOT NULL DEFAULT '1000',
  `Panels` int(11) NOT NULL,
  `Doors` int(11) NOT NULL,
  `Lights` int(11) NOT NULL,
  `Tires` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=230 ;

--
-- Dumping data for table `ownedcars`
--

INSERT INTO `ownedcars` (`id`, `OwnerID`, `ModelID`, `PosX`, `PosY`, `PosZ`, `PosA`, `Interior`, `World`, `Color1`, `Color2`, `Locked`, `Fuel`, `Paintjob`, `XMR`, `Impounded`, `ImpoundPosX`, `ImpoundPosY`, `ImpoundPosZ`, `ImpoundPosA`, `EngineLife`, `BatteryLife`, `LockLevel`, `AlarmLevel`, `ImmobLevel`, `Insurance`, `TimesDestroyed`, `Plate`, `ReportedStolen`, `ReportedStolenDate`, `CarMods1`, `CarMods2`, `CarMods3`, `CarMods4`, `CarMods5`, `CarMods6`, `CarMods7`, `CarMods8`, `CarMods9`, `CarMods10`, `CarMods11`, `CarMods12`, `CarMods13`, `CarMods14`, `Weapons1`, `Weapons2`, `Weapons3`, `Weapons4`, `Weapons5`, `Ammo1`, `Ammo2`, `Ammo3`, `Ammo4`, `Ammo5`, `Packages1`, `Packages2`, `Packages3`, `Packages4`, `Packages5`, `Packages6`, `Packages7`, `Packages8`, `Packages9`, `Packages10`, `Packages11`, `Packages12`, `Packages13`, `Packages14`, `Packages15`, `Packages16`, `Packages17`, `Packages18`, `Packages19`, `Packages20`, `PackagesAmmo1`, `PackagesAmmo2`, `PackagesAmmo3`, `PackagesAmmo4`, `PackagesAmmo5`, `PackagesAmmo6`, `PackagesAmmo7`, `PackagesAmmo8`, `PackagesAmmo9`, `PackagesAmmo10`, `PackagesAmmo11`, `PackagesAmmo12`, `PackagesAmmo13`, `PackagesAmmo14`, `PackagesAmmo15`, `PackagesAmmo16`, `PackagesAmmo17`, `PackagesAmmo18`, `PackagesAmmo19`, `PackagesAmmo20`, `LastDriver`, `LastPassenger`, `drugType1`, `drugType2`, `drugType3`, `drugType4`, `drugType5`, `drugType6`, `drugType7`, `drugType8`, `drugType9`, `drugType10`, `drugType11`, `drugType12`, `drugType13`, `drugType14`, `drugType15`, `drugType16`, `drugType17`, `drugType18`, `drugType19`, `drugType20`, `drugQuantity1`, `drugQuantity2`, `drugQuantity3`, `drugQuantity4`, `drugQuantity5`, `drugQuantity6`, `drugQuantity7`, `drugQuantity8`, `drugQuantity9`, `drugQuantity10`, `drugQuantity11`, `drugQuantity12`, `drugQuantity13`, `drugQuantity14`, `drugQuantity15`, `drugQuantity16`, `drugQuantity17`, `drugQuantity18`, `drugQuantity19`, `drugQuantity20`, `drugType21`, `drugType22`, `drugType23`, `drugType24`, `drugQuantity21`, `drugQuantity22`, `drugQuantity23`, `drugQuantity24`, `Weapon1PosX`, `Weapon1PosY`, `Weapon1PosZ`, `Weapon1RotX`, `Weapon1RotY`, `Weapon1RotZ`, `Weapon2PosX`, `Weapon2PosY`, `Weapon2PosZ`, `Weapon2RotX`, `Weapon2RotY`, `Weapon2RotZ`, `Weapon3PosX`, `Weapon3PosY`, `Weapon3PosZ`, `Weapon3RotX`, `Weapon3RotY`, `Weapon3RotZ`, `Weapon4PosX`, `Weapon4PosY`, `Weapon4PosZ`, `Weapon4RotX`, `Weapon4RotY`, `Weapon4RotZ`, `LastHealth`, `Panels`, `Doors`, `Lights`, `Tires`) VALUES
(226, 604, 426, 1704.13, -1490.29, 14.002, 90, 0, 0, 0, 0, 0, 100, -1, 0, 0, 0, 0, 0, 0, 100, 100, 0, 0, 1, 0, 0, '7DWZ621', 0, '', -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 604, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 1000, 0, 0, 0, 0),
(227, 604, 420, 1704.12, -1490.29, 14.043, 90, 0, 0, 0, 0, 0, 100, -1, 0, 0, 0, 0, 0, 0, 100, 100, 0, 0, 1, 0, 0, '2HOL677', 0, '', -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 604, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 1000, 0, 0, 0, 0),
(228, 604, 540, 1704.26, -1490.3, 14.044, 90, 0, 0, 0, 0, 0, 100, -1, 0, 0, 0, 0, 0, 0, 100, 100, 0, 0, 1, 0, 0, '8GDG511', 0, '', -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 604, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 1000, 0, 0, 0, 0),
(229, 604, 510, 1623.47, -2331.53, 13.128, 289.043, 0, 0, 0, 0, 0, 100, -1, 0, 0, 0, 0, 0, 0, 100, 100, 0, 0, 1, 0, 0, '8BRM652', 0, '', -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 604, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 0, 0, 0.0000009537, 0, 0, 0, 1000, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `paynspray`
--

CREATE TABLE IF NOT EXISTS `paynspray` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `OwnerSQL` int(11) NOT NULL DEFAULT '0',
  `Price` int(11) NOT NULL DEFAULT '1',
  `Name` varchar(128) NOT NULL,
  `EntranceX` float NOT NULL,
  `EntranceY` float NOT NULL,
  `EntranceZ` float NOT NULL,
  `RepairSpotX` float NOT NULL,
  `RepairSpotY` float NOT NULL,
  `RepairSpotZ` float NOT NULL,
  `Earnings` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(32) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `SecretWord` varchar(128) NOT NULL,
  `RegisterIP` varchar(20) NOT NULL,
  `AdminLevel` int(11) NOT NULL DEFAULT '0',
  `Money` int(11) NOT NULL DEFAULT '5000',
  `Level` int(11) NOT NULL DEFAULT '1',
  `Exp` int(11) NOT NULL DEFAULT '0',
  `LastX` float NOT NULL DEFAULT '1642.02',
  `LastY` float NOT NULL DEFAULT '-2334.05',
  `LastZ` float NOT NULL DEFAULT '13.5469',
  `LastRot` float NOT NULL DEFAULT '357.934',
  `Interior` int(11) NOT NULL DEFAULT '0',
  `World` int(11) NOT NULL DEFAULT '0',
  `Skin` int(11) NOT NULL DEFAULT '264',
  `IP` varchar(20) NOT NULL,
  `MaskID` int(11) NOT NULL,
  `MaskIDEx` int(11) NOT NULL,
  `Bank` int(11) NOT NULL DEFAULT '500',
  `Adminjailed` int(11) NOT NULL DEFAULT '0',
  `AjailTime` int(11) NOT NULL DEFAULT '0',
  `MaxHealth` int(11) NOT NULL DEFAULT '100',
  `OwnedCar1` int(11) NOT NULL DEFAULT '0',
  `OwnedCar2` int(11) NOT NULL DEFAULT '0',
  `OwnedCar3` int(11) NOT NULL DEFAULT '0',
  `OwnedCar4` int(11) NOT NULL DEFAULT '0',
  `OwnedCar5` int(11) NOT NULL DEFAULT '0',
  `OwnedCar6` int(11) NOT NULL DEFAULT '0',
  `OwnedCar7` int(11) NOT NULL DEFAULT '0',
  `OwnedCar8` int(11) NOT NULL DEFAULT '0',
  `OwnedCar9` int(11) NOT NULL DEFAULT '0',
  `OwnedCar10` int(11) NOT NULL DEFAULT '0',
  `Renting` int(11) NOT NULL DEFAULT '0',
  `Faction` int(11) NOT NULL DEFAULT '0',
  `FactionRank` int(11) NOT NULL DEFAULT '0',
  `HasRadio` int(11) NOT NULL DEFAULT '0',
  `MainSlot` int(11) DEFAULT '1',
  `Radio1` int(11) NOT NULL DEFAULT '0',
  `Radio2` int(11) NOT NULL DEFAULT '0',
  `Radio3` int(11) NOT NULL DEFAULT '0',
  `Radio4` int(11) NOT NULL DEFAULT '0',
  `Radio5` int(11) NOT NULL DEFAULT '0',
  `Slot1` int(11) NOT NULL DEFAULT '0',
  `Slot2` int(11) NOT NULL DEFAULT '0',
  `Slot3` int(11) NOT NULL DEFAULT '0',
  `Slot4` int(11) NOT NULL DEFAULT '0',
  `Slot5` int(11) NOT NULL DEFAULT '0',
  `Phone` int(11) NOT NULL DEFAULT '0',
  `HasBurnerPhone` int(11) NOT NULL DEFAULT '0',
  `BurnerPhoneNumber` int(11) NOT NULL,
  `Donator` int(11) NOT NULL DEFAULT '0',
  `Tester` int(11) NOT NULL DEFAULT '0',
  `OfflineAjail` int(11) NOT NULL DEFAULT '0',
  `OfflineAjailReason` varchar(128) NOT NULL DEFAULT 'None',
  `HoursPlayed` int(11) NOT NULL DEFAULT '0',
  `Paycheck` int(11) NOT NULL DEFAULT '500',
  `Chatstyle` int(11) NOT NULL DEFAULT '0',
  `Walkstyle` int(11) NOT NULL DEFAULT '0',
  `Savings` int(11) NOT NULL DEFAULT '0',
  `PhoneOff` int(11) NOT NULL DEFAULT '0',
  `InsideComplex` int(11) NOT NULL,
  `InsideHouse` int(11) NOT NULL,
  `InsideApartment` int(11) NOT NULL,
  `InsideBusiness` int(11) NOT NULL,
  `InsideGarage` int(11) NOT NULL DEFAULT '0',
  `WorkOn` int(11) NOT NULL DEFAULT '0',
  `DriversLicense` int(11) NOT NULL DEFAULT '0',
  `WeaponsLicense` int(11) NOT NULL DEFAULT '0',
  `SpawnSelect` int(11) NOT NULL DEFAULT '0',
  `SpawnHouse` int(11) NOT NULL DEFAULT '0',
  `Crashed` int(11) NOT NULL DEFAULT '0',
  `Playtime` int(11) NOT NULL DEFAULT '0',
  `HasCarSpawned` int(11) NOT NULL DEFAULT '0',
  `HasCarSpawnedID` int(11) NOT NULL DEFAULT '0',
  `AdminMessage` varchar(128) NOT NULL,
  `AdminMessageBy` varchar(60) NOT NULL,
  `AdmMessageConfirm` int(11) NOT NULL DEFAULT '0',
  `UpgradePoints` int(11) NOT NULL DEFAULT '0',
  `Package1` int(11) NOT NULL,
  `Package1A` int(11) NOT NULL,
  `Package2` int(11) NOT NULL,
  `Package2A` int(11) NOT NULL,
  `Package3` int(11) NOT NULL,
  `Package3A` int(11) NOT NULL,
  `Package4` int(11) NOT NULL,
  `Package4A` int(11) NOT NULL,
  `Package5` int(11) NOT NULL,
  `Package5A` int(11) NOT NULL,
  `Package6` int(11) NOT NULL,
  `Package6A` int(11) NOT NULL,
  `Package7` int(11) NOT NULL,
  `Package7A` int(11) NOT NULL,
  `Package8` int(11) NOT NULL,
  `Package8A` int(11) NOT NULL,
  `Package9` int(11) NOT NULL,
  `Package9A` int(11) NOT NULL,
  `Package10` int(11) NOT NULL,
  `Package10A` int(11) NOT NULL,
  `Package11` int(11) NOT NULL,
  `Package11A` int(11) NOT NULL,
  `Package12` int(11) NOT NULL,
  `Package12A` int(11) NOT NULL,
  `Package13` int(11) NOT NULL,
  `Package13A` int(11) NOT NULL,
  `Package14` int(11) NOT NULL,
  `Package14A` int(11) NOT NULL,
  `Package15` int(11) NOT NULL,
  `Package15A` int(11) NOT NULL,
  `Package16` int(11) NOT NULL,
  `Package16A` int(11) NOT NULL,
  `Package17` int(11) NOT NULL,
  `Package17A` int(11) NOT NULL,
  `Package18` int(11) NOT NULL,
  `Package18A` int(11) NOT NULL,
  `Package19` int(11) NOT NULL,
  `Package19A` int(11) NOT NULL,
  `Package20` int(11) NOT NULL,
  `Package20A` int(11) NOT NULL,
  `Job` int(11) NOT NULL DEFAULT '0',
  `Sidejob` int(11) NOT NULL DEFAULT '0',
  `drugQuantity1` int(11) NOT NULL,
  `drugQuantity2` int(11) NOT NULL,
  `drugQuantity3` int(11) NOT NULL,
  `drugQuantity4` int(11) NOT NULL,
  `drugQuantity5` int(11) NOT NULL,
  `drugQuantity6` int(11) NOT NULL,
  `drugQuantity7` int(11) NOT NULL,
  `drugQuantity8` int(11) NOT NULL,
  `drugQuantity9` int(11) NOT NULL,
  `drugQuantity10` int(11) NOT NULL,
  `drugQuantity11` int(11) NOT NULL,
  `drugQuantity12` int(11) NOT NULL,
  `drugQuantity13` int(11) NOT NULL,
  `drugQuantity14` int(11) NOT NULL,
  `drugQuantity15` int(11) NOT NULL,
  `drugQuantity16` int(11) NOT NULL,
  `drugQuantity17` int(11) NOT NULL,
  `drugQuantity18` int(11) NOT NULL,
  `drugQuantity19` int(11) NOT NULL,
  `drugQuantity20` int(11) NOT NULL,
  `drugQuantity21` int(11) NOT NULL,
  `drugQuantity22` int(11) NOT NULL,
  `drugQuantity23` int(11) NOT NULL,
  `drugQuantity24` int(11) NOT NULL,
  `drugType1` int(11) NOT NULL,
  `drugType2` int(11) NOT NULL,
  `drugType3` int(11) NOT NULL,
  `drugType4` int(11) NOT NULL,
  `drugType5` int(11) NOT NULL,
  `drugType6` int(11) NOT NULL,
  `drugType7` int(11) NOT NULL,
  `drugType8` int(11) NOT NULL,
  `drugType9` int(11) NOT NULL,
  `drugType10` int(11) NOT NULL,
  `drugType11` int(11) NOT NULL,
  `drugType12` int(11) NOT NULL,
  `drugType13` int(11) NOT NULL,
  `drugType14` int(11) NOT NULL,
  `drugType15` int(11) NOT NULL,
  `drugType16` int(11) NOT NULL,
  `drugType17` int(11) NOT NULL,
  `drugType18` int(11) NOT NULL,
  `drugType19` int(11) NOT NULL,
  `drugType20` int(11) NOT NULL,
  `drugType21` int(11) NOT NULL,
  `drugType22` int(11) NOT NULL,
  `drugType23` int(11) NOT NULL,
  `drugType24` int(11) NOT NULL,
  `LastConnection` varchar(60) NOT NULL,
  `LastConnectLength` int(11) NOT NULL,
  `HWID` varchar(60) NOT NULL,
  `LastHealth` float NOT NULL DEFAULT '100',
  `LastArmor` float NOT NULL DEFAULT '0',
  `ActiveListing` int(11) NOT NULL DEFAULT '0',
  `JailTimes` int(11) NOT NULL DEFAULT '0',
  `PrisonTimes` int(11) NOT NULL DEFAULT '0',
  `pCopDuty` int(11) NOT NULL DEFAULT '0',
  `pSWATDuty` int(11) NOT NULL DEFAULT '0',
  `pCrashTime` int(11) NOT NULL DEFAULT '0',
  `OrderWeaponRestricted` int(11) NOT NULL DEFAULT '0',
  `OrderWeaponTime` int(11) NOT NULL DEFAULT '0',
  `ICJailed` int(11) NOT NULL DEFAULT '0',
  `ICJailTime` int(11) NOT NULL DEFAULT '0',
  `Cigarettes` int(11) NOT NULL DEFAULT '0',
  `Drinks` int(11) NOT NULL DEFAULT '0',
  `ForumName` varchar(60) NOT NULL,
  `SpawnPrecinct` int(11) NOT NULL DEFAULT '0',
  `BrutallyWounded` int(11) NOT NULL DEFAULT '0',
  `DCToggle` int(11) NOT NULL DEFAULT '0',
  `HUD` int(11) NOT NULL DEFAULT '0',
  `HUDToggled` int(11) NOT NULL DEFAULT '0',
  `Fightstyle` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=611 ;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `Name`, `Password`, `SecretWord`, `RegisterIP`, `AdminLevel`, `Money`, `Level`, `Exp`, `LastX`, `LastY`, `LastZ`, `LastRot`, `Interior`, `World`, `Skin`, `IP`, `MaskID`, `MaskIDEx`, `Bank`, `Adminjailed`, `AjailTime`, `MaxHealth`, `OwnedCar1`, `OwnedCar2`, `OwnedCar3`, `OwnedCar4`, `OwnedCar5`, `OwnedCar6`, `OwnedCar7`, `OwnedCar8`, `OwnedCar9`, `OwnedCar10`, `Renting`, `Faction`, `FactionRank`, `HasRadio`, `MainSlot`, `Radio1`, `Radio2`, `Radio3`, `Radio4`, `Radio5`, `Slot1`, `Slot2`, `Slot3`, `Slot4`, `Slot5`, `Phone`, `HasBurnerPhone`, `BurnerPhoneNumber`, `Donator`, `Tester`, `OfflineAjail`, `OfflineAjailReason`, `HoursPlayed`, `Paycheck`, `Chatstyle`, `Walkstyle`, `Savings`, `PhoneOff`, `InsideComplex`, `InsideHouse`, `InsideApartment`, `InsideBusiness`, `InsideGarage`, `WorkOn`, `DriversLicense`, `WeaponsLicense`, `SpawnSelect`, `SpawnHouse`, `Crashed`, `Playtime`, `HasCarSpawned`, `HasCarSpawnedID`, `AdminMessage`, `AdminMessageBy`, `AdmMessageConfirm`, `UpgradePoints`, `Package1`, `Package1A`, `Package2`, `Package2A`, `Package3`, `Package3A`, `Package4`, `Package4A`, `Package5`, `Package5A`, `Package6`, `Package6A`, `Package7`, `Package7A`, `Package8`, `Package8A`, `Package9`, `Package9A`, `Package10`, `Package10A`, `Package11`, `Package11A`, `Package12`, `Package12A`, `Package13`, `Package13A`, `Package14`, `Package14A`, `Package15`, `Package15A`, `Package16`, `Package16A`, `Package17`, `Package17A`, `Package18`, `Package18A`, `Package19`, `Package19A`, `Package20`, `Package20A`, `Job`, `Sidejob`, `drugQuantity1`, `drugQuantity2`, `drugQuantity3`, `drugQuantity4`, `drugQuantity5`, `drugQuantity6`, `drugQuantity7`, `drugQuantity8`, `drugQuantity9`, `drugQuantity10`, `drugQuantity11`, `drugQuantity12`, `drugQuantity13`, `drugQuantity14`, `drugQuantity15`, `drugQuantity16`, `drugQuantity17`, `drugQuantity18`, `drugQuantity19`, `drugQuantity20`, `drugQuantity21`, `drugQuantity22`, `drugQuantity23`, `drugQuantity24`, `drugType1`, `drugType2`, `drugType3`, `drugType4`, `drugType5`, `drugType6`, `drugType7`, `drugType8`, `drugType9`, `drugType10`, `drugType11`, `drugType12`, `drugType13`, `drugType14`, `drugType15`, `drugType16`, `drugType17`, `drugType18`, `drugType19`, `drugType20`, `drugType21`, `drugType22`, `drugType23`, `drugType24`, `LastConnection`, `LastConnectLength`, `HWID`, `LastHealth`, `LastArmor`, `ActiveListing`, `JailTimes`, `PrisonTimes`, `pCopDuty`, `pSWATDuty`, `pCrashTime`, `OrderWeaponRestricted`, `OrderWeaponTime`, `ICJailed`, `ICJailTime`, `Cigarettes`, `Drinks`, `ForumName`, `SpawnPrecinct`, `BrutallyWounded`, `DCToggle`, `HUD`, `HUDToggled`, `Fightstyle`) VALUES
(1, 'Frank_Herd', '403926033d001b5279df37cbbe5287b7c7c267fa', '403926033d001b5279df37cbbe5287b7c7c267fa', '1.1.1.1.1', 1338, 12848, 2, 13, 1759.71, 707.56, 10.82, 9.156, 0, 0, 20, '1.1.1.1.1', 289498, 80, 519, 0, 0, 100, 226, 227, 228, 229, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 94068, 0, 42300, 0, 0, 0, 'None', 19, 1200, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 574, 0, 0, '', '', 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'October 25, 2017 10:41:05', 9, 'C8C0884F48CE88A9C849EE5F99A449EE8CA88E58', 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'ff', 0, 0, 0, 0, 0, 0),
(610, 'Samuel_Morris', 'c3c3707c81aeb1b5c623d297ffffe7697fa9ead2', 'c3c3707c81aeb1b5c623d297ffffe7697fa9ead2', '151.231.230.165', 6, 4700, 1, 0, 972.415, -1390.82, 13.039, 244.853, 0, 0, 286, '151.231.230.165', 202233, 40, 500, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 94555, 0, 36846, 4, 0, 0, 'None', 0, 500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1010, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'October 25, 2017 18:08:44', 45, 'CEFA4AA85AFC4C0F49ACE459D590AC4FDEDEF8E5', 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'James', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `player_anotes`
--

CREATE TABLE IF NOT EXISTS `player_anotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_dbid` int(11) NOT NULL,
  `anote_reason` varchar(128) NOT NULL,
  `anote_issuer` varchar(60) NOT NULL,
  `anote_date` varchar(90) NOT NULL,
  `anote_active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `player_clothing`
--

CREATE TABLE IF NOT EXISTS `player_clothing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_dbid` int(11) NOT NULL,
  `modelid` int(11) NOT NULL,
  `bone` int(11) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `RotX` float NOT NULL,
  `RotY` float NOT NULL,
  `RotZ` float NOT NULL,
  `ScaleX` float NOT NULL,
  `ScaleY` float NOT NULL,
  `ScaleZ` float NOT NULL,
  `name` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `player_contacts`
--

CREATE TABLE IF NOT EXISTS `player_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playersqlid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL,
  `contact_name` varchar(128) NOT NULL,
  `contact_num` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `player_fines`
--

CREATE TABLE IF NOT EXISTS `player_fines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_dbid` int(11) NOT NULL,
  `issuer_name` varchar(34) NOT NULL,
  `fine_amount` int(11) NOT NULL,
  `fine_reason` varchar(128) NOT NULL,
  `fine_date` varchar(90) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `player_fines_ibfk_1` (`player_dbid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `player_logs`
--

CREATE TABLE IF NOT EXISTS `player_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `player_dbid` int(11) NOT NULL,
  `log_detail` varchar(128) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9077 ;

--
-- Dumping data for table `player_logs`
--

INSERT INTO `player_logs` (`log_id`, `player_dbid`, `log_detail`) VALUES
(8789, 604, '2017-10-20 05:48:14 Disconnected by Quitting'),
(8790, 605, '2017-10-20 05:49:20 Disconnected by Quitting'),
(8791, 604, '2017-10-20 05:54:45 Killed Ryan_Everett with M4'),
(8792, 605, '2017-10-20 05:54:45 Killed by Frank_Herd with M4'),
(8793, 604, '2017-10-20 06:01:11 Killed Ryan_Everett with M4'),
(8794, 605, '2017-10-20 06:01:11 Killed by Frank_Herd with M4'),
(8795, 604, '2017-10-20 06:01:32 Disconnected by Quitting'),
(8796, 605, '2017-10-20 06:01:32 Disconnected by Quitting'),
(8797, 604, '2017-10-20 06:01:57 Killed Ryan_Everett with M4'),
(8798, 605, '2017-10-20 06:01:57 Killed by Frank_Herd with M4'),
(8799, 604, '2017-10-20 06:02:42 Killed Ryan_Everett with Desert Eagle'),
(8800, 605, '2017-10-20 06:02:42 Killed by Frank_Herd with Desert Eagle'),
(8801, 604, '2017-10-20 06:13:03 Killed Ryan_Everett with AK47'),
(8802, 605, '2017-10-20 06:13:03 Killed by Frank_Herd with AK47'),
(8803, 604, '2017-10-20 06:13:05 Disconnected by Quitting'),
(8804, 605, '2017-10-20 06:13:05 Disconnected by Quitting'),
(8805, 605, '2017-10-20 06:13:31 Executed by Frank_Herd with AK47'),
(8806, 604, '2017-10-20 06:13:31 Executed Ryan_Everett with AK47'),
(8807, 604, '2017-10-20 06:15:13 Lost Nite Stick and 1 Ammo'),
(8808, 604, '2017-10-20 06:15:13 Lost Spray Can and 350 Ammo'),
(8809, 604, '2017-10-20 06:15:13 Lost Desert Eagle and 100 Ammo'),
(8810, 604, '2017-10-20 06:15:13 Lost AK47 and 32 Ammo'),
(8811, 605, '2017-10-20 06:15:46 Lost Nite Stick and 1 Ammo'),
(8812, 605, '2017-10-20 06:15:46 Lost Spray Can and 350 Ammo'),
(8813, 605, '2017-10-20 06:15:46 Lost Desert Eagle and 100 Ammo'),
(8814, 605, '2017-10-20 06:15:46 Lost Shotgun and 3 Ammo'),
(8815, 604, '2017-10-20 06:15:46 Killed Ryan_Everett with Shotgun'),
(8816, 605, '2017-10-20 06:15:46 Killed by Frank_Herd with Shotgun'),
(8817, 604, '2017-10-20 06:16:02 Lost Nite Stick and 1 Ammo'),
(8818, 604, '2017-10-20 06:16:02 Lost Spray Can and 350 Ammo'),
(8819, 604, '2017-10-20 06:16:02 Lost Desert Eagle and 100 Ammo'),
(8820, 604, '2017-10-20 06:16:02 Lost Shotgun and 94 Ammo'),
(8821, 605, '2017-10-20 06:16:20 Rubberbulleted by Frank_Herd'),
(8822, 604, '2017-10-20 06:16:20 Rubberbulleted player Ryan_Everett'),
(8823, 605, '2017-10-20 06:16:20 Rubberbulleted by Frank_Herd'),
(8824, 604, '2017-10-20 06:16:20 Rubberbulleted player Ryan_Everett'),
(8825, 605, '2017-10-20 06:16:24 Rubberbulleted by Frank_Herd'),
(8826, 604, '2017-10-20 06:16:24 Rubberbulleted player Ryan_Everett'),
(8827, 605, '2017-10-20 06:16:24 Rubberbulleted by Frank_Herd'),
(8828, 604, '2017-10-20 06:16:24 Rubberbulleted player Ryan_Everett'),
(8829, 605, '2017-10-20 06:16:32 Rubberbulleted by Frank_Herd'),
(8830, 604, '2017-10-20 06:16:32 Rubberbulleted player Ryan_Everett'),
(8831, 605, '2017-10-20 06:16:32 Rubberbulleted by Frank_Herd'),
(8832, 604, '2017-10-20 06:16:32 Rubberbulleted player Ryan_Everett'),
(8833, 605, '2017-10-20 06:16:57 Tasered by Frank_Herd'),
(8834, 604, '2017-10-20 06:16:57 Tased player Ryan_Everett'),
(8835, 605, '2017-10-20 06:16:57 Tasered by Frank_Herd'),
(8836, 604, '2017-10-20 06:16:57 Tased player Ryan_Everett'),
(8837, 605, '2017-10-20 06:17:01 Tasered by Frank_Herd'),
(8838, 604, '2017-10-20 06:17:01 Tased player Ryan_Everett'),
(8839, 605, '2017-10-20 06:17:01 Tasered by Frank_Herd'),
(8840, 604, '2017-10-20 06:17:01 Tased player Ryan_Everett'),
(8841, 605, '2017-10-20 06:17:03 Tasered by Frank_Herd'),
(8842, 604, '2017-10-20 06:17:03 Tased player Ryan_Everett'),
(8843, 605, '2017-10-20 06:17:03 Tasered by Frank_Herd'),
(8844, 604, '2017-10-20 06:17:03 Tased player Ryan_Everett'),
(8845, 605, '2017-10-20 06:17:05 Tasered by Frank_Herd'),
(8846, 604, '2017-10-20 06:17:05 Tased player Ryan_Everett'),
(8847, 605, '2017-10-20 06:17:05 Tasered by Frank_Herd'),
(8848, 604, '2017-10-20 06:17:05 Tased player Ryan_Everett'),
(8849, 605, '2017-10-20 06:17:06 Tasered by Frank_Herd'),
(8850, 604, '2017-10-20 06:17:06 Tased player Ryan_Everett'),
(8851, 605, '2017-10-20 06:17:06 Tasered by Frank_Herd'),
(8852, 604, '2017-10-20 06:17:06 Tased player Ryan_Everett'),
(8853, 604, '2017-10-20 06:17:33 Killed Ryan_Everett with Desert Eagle'),
(8854, 605, '2017-10-20 06:17:33 Killed by Frank_Herd with Desert Eagle'),
(8855, 605, '2017-10-20 06:17:37 Executed by Frank_Herd with Desert Eagle'),
(8856, 604, '2017-10-20 06:17:37 Executed Ryan_Everett with Desert Eagle'),
(8857, 604, '2017-10-20 06:23:28 Lost Nite Stick and 1 Ammo'),
(8858, 604, '2017-10-20 06:23:28 Lost Spray Can and 350 Ammo'),
(8859, 604, '2017-10-20 06:23:28 Lost Desert Eagle and 60 Ammo'),
(8860, 605, '2017-10-20 06:23:28 Killed Frank_Herd with Desert Eagle'),
(8861, 604, '2017-10-20 06:23:28 Killed by Ryan_Everett with Desert Eagle'),
(8862, 604, '2017-10-20 06:43:24 Disconnected by Quitting'),
(8863, 604, '2017-10-20 07:09:21 Disconnected by Quitting'),
(8864, 604, '2017-10-20 07:17:50 Disconnected by Quitting'),
(8865, 604, '2017-10-20 07:21:32 Disconnected by Quitting'),
(8866, 604, '2017-10-20 07:37:43 Disconnected by Quitting'),
(8867, 604, '2017-10-20 08:01:57 Disconnected by Quitting'),
(8868, 606, '2017-10-20 08:36:05 Disconnected by Quitting'),
(8869, 604, '2017-10-20 08:44:28 Disconnected by Quitting'),
(8870, 604, '2017-10-20 19:10:16 Lost Desert Eagle and 55 Ammo'),
(8871, 605, '2017-10-20 19:10:16 Killed Frank_Herd with M4'),
(8872, 604, '2017-10-20 19:10:16 Killed by Ryan_Everett with M4'),
(8873, 605, '2017-10-20 19:12:46 Lost Brass Knuckles and 1 Ammo'),
(8874, 605, '2017-10-20 19:12:46 Lost M4 and 273 Ammo'),
(8875, 604, '2017-10-20 19:14:34 Disconnected by Quitting'),
(8876, 605, '2017-10-20 19:14:34 Disconnected by Quitting'),
(8877, 604, '2017-10-20 19:17:20 Killed Ryan_Everett with AK47'),
(8878, 605, '2017-10-20 19:17:20 Killed by Frank_Herd with AK47'),
(8879, 604, '2017-10-20 19:17:36 Killed Ryan_Everett with AK47'),
(8880, 605, '2017-10-20 19:17:36 Killed by Frank_Herd with AK47'),
(8881, 604, '2017-10-20 19:18:22 Lost Desert Eagle and 8 Ammo'),
(8882, 604, '2017-10-20 19:18:22 Lost AK47 and 99 Ammo'),
(8883, 605, '2017-10-20 19:18:22 Killed Frank_Herd with Sniper Rifle'),
(8884, 604, '2017-10-20 19:18:22 Killed by Ryan_Everett with Sniper Rifle'),
(8885, 604, '2017-10-20 19:18:42 Executed by Ryan_Everett with Sniper Rifle'),
(8886, 605, '2017-10-20 19:18:42 Executed Frank_Herd with Sniper Rifle'),
(8887, 604, '2017-10-20 19:26:57 Disconnected by Quitting'),
(8888, 605, '2017-10-20 19:26:57 Disconnected by Quitting'),
(8889, 604, '2017-10-20 19:27:30 Killed Ryan_Everett with Desert Eagle'),
(8890, 605, '2017-10-20 19:27:30 Killed by Frank_Herd with Desert Eagle'),
(8891, 604, '2017-10-20 19:29:51 Killed Ryan_Everett with M4'),
(8892, 605, '2017-10-20 19:29:51 Killed by Frank_Herd with M4'),
(8893, 605, '2017-10-20 19:29:55 Executed by Frank_Herd with M4'),
(8894, 604, '2017-10-20 19:29:55 Executed Ryan_Everett with M4'),
(8895, 604, '2017-10-20 19:32:28 Disconnected by Quitting'),
(8896, 605, '2017-10-20 19:32:28 Disconnected by Quitting'),
(8897, 604, '2017-10-20 19:33:04 Lost Desert Eagle and 92 Ammo'),
(8898, 604, '2017-10-20 19:33:04 Lost TEC9 and 100 Ammo'),
(8899, 604, '2017-10-20 19:37:06 Disconnected by Quitting'),
(8900, 605, '2017-10-20 19:37:06 Disconnected by Quitting'),
(8901, 604, '2017-10-20 19:37:45 Lost M4 and 77 Ammo'),
(8902, 605, '2017-10-20 19:37:45 Killed Frank_Herd with Desert Eagle'),
(8903, 604, '2017-10-20 19:37:45 Killed by Ryan_Everett with Desert Eagle'),
(8904, 605, '2017-10-20 19:38:10 Lost Desert Eagle and 89 Ammo'),
(8905, 604, '2017-10-20 19:38:10 Killed Ryan_Everett with Shotgun'),
(8906, 605, '2017-10-20 19:38:10 Killed by Frank_Herd with Shotgun'),
(8907, 604, '2017-10-20 19:43:20 Disconnected by Quitting'),
(8908, 605, '2017-10-20 19:43:20 Disconnected by Quitting'),
(8909, 605, '2017-10-20 19:48:37 Lost Desert Eagle and 97 Ammo'),
(8910, 604, '2017-10-20 19:48:37 Killed Ryan_Everett with M4'),
(8911, 605, '2017-10-20 19:48:37 Killed by Frank_Herd with M4'),
(8912, 604, '2017-10-20 19:50:39 Disconnected by Quitting'),
(8913, 605, '2017-10-20 19:50:39 Disconnected by Quitting'),
(8914, 604, '2017-10-20 19:51:08 Lost ''\\ and 39 Ammo'),
(8915, 604, '2017-10-20 19:53:00 Killed Ryan_Everett with M4'),
(8916, 605, '2017-10-20 19:53:00 Killed by Frank_Herd with M4'),
(8917, 605, '2017-10-20 19:58:13 Rubberbulleted by Frank_Herd'),
(8918, 604, '2017-10-20 19:58:13 Rubberbulleted player Ryan_Everett'),
(8919, 605, '2017-10-20 19:58:16 Rubberbulleted by Frank_Herd'),
(8920, 604, '2017-10-20 19:58:16 Rubberbulleted player Ryan_Everett'),
(8921, 605, '2017-10-20 19:58:31 Rubberbulleted by Frank_Herd'),
(8922, 604, '2017-10-20 19:58:31 Rubberbulleted player Ryan_Everett'),
(8923, 605, '2017-10-20 19:58:44 Tasered by Frank_Herd'),
(8924, 604, '2017-10-20 19:58:44 Tased player Ryan_Everett'),
(8925, 604, '2017-10-20 19:59:55 Disconnected by Quitting'),
(8926, 605, '2017-10-20 19:59:55 Disconnected by Quitting'),
(8927, 604, '2017-10-20 20:00:15 Lost M4 and 2 Ammo'),
(8928, 605, '2017-10-20 20:00:32 Lost Brass Knuckles and 1 Ammo'),
(8929, 604, '2017-10-20 20:00:32 Killed Ryan_Everett with M4'),
(8930, 605, '2017-10-20 20:00:32 Killed by Frank_Herd with M4'),
(8931, 604, '2017-10-20 20:00:40 Killed Ryan_Everett with M4'),
(8932, 605, '2017-10-20 20:00:40 Killed by Frank_Herd with M4'),
(8933, 604, '2017-10-20 20:01:08 Killed Ryan_Everett with Shotgun'),
(8934, 605, '2017-10-20 20:01:08 Killed by Frank_Herd with Shotgun'),
(8935, 604, '2017-10-20 20:01:19 Killed Ryan_Everett with Shotgun'),
(8936, 605, '2017-10-20 20:01:19 Killed by Frank_Herd with Shotgun'),
(8937, 604, '2017-10-20 20:01:50 Killed Ryan_Everett with Shotgun'),
(8938, 605, '2017-10-20 20:01:50 Killed by Frank_Herd with Shotgun'),
(8939, 605, '2017-10-20 20:02:14 Executed by Frank_Herd with Shotgun'),
(8940, 604, '2017-10-20 20:02:14 Executed Ryan_Everett with Shotgun'),
(8941, 605, '2017-10-20 20:04:24 Disconnected by Quitting'),
(8942, 604, '2017-10-20 20:05:22 Lost Shotgun and 65 Ammo'),
(8943, 604, '2017-10-20 20:05:28 Disconnected by Quitting'),
(8944, 604, '2017-10-20 20:05:47 Lost M4 and 100 Ammo'),
(8945, 604, '2017-10-20 20:33:06 Disconnected by Quitting'),
(8946, 604, '2017-10-20 20:43:30 Disconnected by Quitting'),
(8947, 604, '2017-10-20 20:56:34 Disconnected by Quitting'),
(8948, 604, '2017-10-20 21:05:32 Disconnected by Quitting'),
(8949, 604, '2017-10-20 21:15:45 Disconnected by Quitting'),
(8950, 604, '2017-10-20 21:32:50 Disconnected by Quitting'),
(8951, 604, '2017-10-21 07:07:18 Disconnected by Quitting'),
(8952, 604, '2017-10-21 07:13:06 Disconnected by Quitting'),
(8953, 604, '2017-10-21 07:18:27 Disconnected by Quitting'),
(8954, 604, '2017-10-21 07:22:24 Disconnected by Quitting'),
(8955, 604, '2017-10-21 07:25:09 Disconnected by Quitting'),
(8956, 604, '2017-10-21 07:28:03 Disconnected by Quitting'),
(8957, 604, '2017-10-21 07:31:45 Disconnected by Quitting'),
(8958, 604, '2017-10-21 07:34:53 Disconnected by Quitting'),
(8959, 604, '2017-10-21 07:46:11 Disconnected by Quitting'),
(8960, 604, '2017-10-21 07:50:42 Disconnected by Quitting'),
(8961, 604, '2017-10-21 07:55:39 Disconnected by Quitting'),
(8962, 604, '2017-10-21 07:58:31 Disconnected by Quitting'),
(8963, 604, '2017-10-21 08:09:18 Disconnected by Quitting'),
(8964, 604, '2017-10-21 08:14:46 Disconnected by Quitting'),
(8965, 604, '2017-10-21 08:19:34 Disconnected by Quitting'),
(8966, 604, '2017-10-21 08:21:55 Disconnected by Quitting'),
(8967, 604, '2017-10-21 08:30:52 Disconnected by Quitting'),
(8968, 604, '2017-10-21 08:35:10 Disconnected by Quitting'),
(8969, 604, '2017-10-21 08:37:54 Disconnected by Quitting'),
(8970, 604, '2017-10-21 08:53:47 Disconnected by Quitting'),
(8971, 604, '2017-10-21 09:01:01 Disconnected by Quitting'),
(8972, 604, '2017-10-21 09:02:55 Disconnected by Quitting'),
(8973, 604, '2017-10-21 10:01:52 Disconnected by Quitting'),
(8974, 604, '2017-10-21 10:07:13 Disconnected by Quitting'),
(8975, 604, '2017-10-21 10:47:27 Disconnected by Quitting'),
(8976, 607, '2017-10-21 13:06:44 Disconnected by Quitting'),
(8977, 604, '2017-10-21 13:12:00 Disconnected by Quitting'),
(8978, 604, '2017-10-21 13:13:21 Disconnected by Quitting'),
(8979, 604, '2017-10-21 13:14:17 Disconnected by Quitting'),
(8980, 604, '2017-10-21 13:15:11 Disconnected by Quitting'),
(8981, 604, '2017-10-21 13:22:33 Disconnected by Quitting'),
(8982, 604, '2017-10-21 13:31:47 Disconnected by Quitting'),
(8983, 604, '2017-10-21 13:34:32 Disconnected by Quitting'),
(8984, 604, '2017-10-21 13:38:11 Disconnected by Quitting'),
(8985, 604, '2017-10-21 13:42:36 Disconnected by Quitting'),
(8986, 604, '2017-10-21 23:46:55 Disconnected by Quitting'),
(8987, 604, '2017-10-21 23:55:38 Disconnected by Quitting'),
(8988, 604, '2017-10-21 23:58:02 Disconnected by Quitting'),
(8989, 604, '2017-10-22 00:06:06 Disconnected by Quitting'),
(8990, 604, '2017-10-22 00:16:05 Disconnected by Quitting'),
(8991, 604, '2017-10-22 00:21:50 Disconnected by Quitting'),
(8992, 604, '2017-10-22 00:28:30 Disconnected by Quitting'),
(8993, 604, '2017-10-22 00:33:47 Disconnected by Quitting'),
(8994, 604, '2017-10-22 00:44:15 Disconnected by Quitting'),
(8995, 604, '2017-10-22 00:48:28 Disconnected by Quitting'),
(8996, 604, '2017-10-22 01:04:33 Disconnected by Quitting'),
(8997, 604, '2017-10-22 01:06:08 Disconnected by Quitting'),
(8998, 604, '2017-10-22 02:38:05 Disconnected by Quitting'),
(8999, 604, '2017-10-22 02:43:57 Disconnected by Quitting'),
(9000, 604, '2017-10-22 02:54:51 Disconnected by Quitting'),
(9001, 604, '2017-10-22 05:35:38 Disconnected by Quitting'),
(9002, 604, '2017-10-22 05:48:59 Disconnected by Quitting'),
(9003, 604, '2017-10-22 05:53:33 Disconnected by Quitting'),
(9004, 604, '2017-10-22 05:56:49 Disconnected by Quitting'),
(9005, 604, '2017-10-22 06:20:17 Disconnected by Quitting'),
(9006, 604, '2017-10-22 09:31:38 Disconnected by Quitting'),
(9007, 604, '2017-10-22 09:37:29 Disconnected by Quitting'),
(9008, 604, '2017-10-22 09:48:13 Disconnected by Quitting'),
(9009, 604, '2017-10-22 11:13:17 Disconnected by Quitting'),
(9010, 604, '2017-10-22 11:16:02 Disconnected by Quitting'),
(9011, 604, '2017-10-22 11:18:58 Disconnected by Quitting'),
(9012, 604, '2017-10-22 11:28:39 Disconnected by Quitting'),
(9013, 604, '2017-10-22 11:31:03 Disconnected by Quitting'),
(9014, 604, '2017-10-22 11:41:21 Disconnected by Quitting'),
(9015, 608, '2017-10-22 11:41:21 Disconnected by Quitting'),
(9016, 604, '2017-10-22 12:04:27 Disconnected by Quitting'),
(9017, 604, '2017-10-22 12:08:22 Disconnected by Quitting'),
(9018, 604, '2017-10-22 12:09:16 Disconnected by Quitting'),
(9019, 604, '2017-10-22 12:15:11 Disconnected by Quitting'),
(9020, 604, '2017-10-22 12:21:18 Disconnected by Quitting'),
(9021, 604, '2017-10-22 12:25:20 Disconnected by Quitting'),
(9022, 604, '2017-10-22 12:27:51 Placed Marijuana(200) in Property 1'),
(9023, 604, '2017-10-22 12:37:39 Disconnected by Quitting'),
(9024, 604, '2017-10-22 12:38:23 Placed Ecstasy(100) in Property 1'),
(9025, 604, '2017-10-22 13:09:14 Disconnected by Quitting'),
(9026, 604, '2017-10-22 13:10:05 Placed Ecstasy(50) in Property 1'),
(9027, 604, '2017-10-22 13:10:29 Took Ecstasy(50) from Property 1'),
(9028, 604, '2017-10-22 13:11:04 Placed Ecstasy(40) in Property 1'),
(9029, 604, '2017-10-22 13:32:52 Disconnected by Quitting'),
(9030, 604, '2017-10-22 13:39:50 Disconnected by Quitting'),
(9031, 604, '2017-10-22 13:43:09 Disconnected by Quitting'),
(9032, 604, '2017-10-22 13:49:47 Disconnected by Quitting'),
(9033, 604, '2017-10-23 03:36:35 Disconnected by Quitting'),
(9034, 604, '2017-10-23 04:01:52 Disconnected by Quitting'),
(9035, 604, '2017-10-23 04:07:38 Disconnected by Quitting'),
(9036, 609, '2017-10-23 04:12:30 Killed Frank_Herd with Desert Eagle'),
(9037, 604, '2017-10-23 04:12:30 Killed by Richard_Williamson with Desert Eagle'),
(9038, 609, '2017-10-23 04:15:31 Disconnected by Quitting'),
(9039, 604, '2017-10-23 04:30:39 Disconnected by Quitting'),
(9040, 604, '2017-10-23 04:31:31 Disconnected by Quitting'),
(9041, 1, '2017-10-23 04:35:10 Disconnected by Quitting'),
(9042, 1, '2017-10-23 04:37:35 Disconnected by Quitting'),
(9043, 1, '2017-10-23 04:41:28 Disconnected by Quitting'),
(9044, 1, '2017-10-23 04:53:07 Disconnected by Quitting'),
(9045, 1, '2017-10-23 23:17:31 Disconnected by Quitting'),
(9046, 1, '2017-10-23 23:23:34 Disconnected by Quitting'),
(9047, 1, '2017-10-23 23:25:01 Disconnected by Quitting'),
(9048, 1, '2017-10-23 23:29:42 Disconnected by Quitting'),
(9049, 1, '2017-10-23 23:38:45 Disconnected by Quitting'),
(9050, 1, '2017-10-23 23:48:18 Disconnected by Quitting'),
(9051, 610, '2017-10-24 22:36:00 Disconnected by Quitting'),
(9052, 610, '2017-10-24 22:43:29 Rented Rental Vehicle DBID 1'),
(9053, 610, '2017-10-24 22:53:44 Disconnected by Quitting'),
(9054, 1, '2017-10-25 04:10:58 Disconnected by Quitting'),
(9055, 1, '2017-10-25 04:13:34 Disconnected by Quitting'),
(9056, 1, '2017-10-25 04:24:26 Disconnected by Quitting'),
(9057, 1, '2017-10-25 04:28:12 Disconnected by Quitting'),
(9058, 1, '2017-10-25 04:52:56 Disconnected by Quitting'),
(9059, 1, '2017-10-25 08:39:09 Disconnected by Quitting'),
(9060, 1, '2017-10-25 08:45:29 Disconnected by Quitting'),
(9061, 1, '2017-10-25 08:53:48 Disconnected by Quitting'),
(9062, 1, '2017-10-25 08:55:41 Disconnected by Quitting'),
(9063, 1, '2017-10-25 09:02:08 Disconnected by Quitting'),
(9064, 1, '2017-10-25 09:11:24 Disconnected by Quitting'),
(9065, 1, '2017-10-25 09:15:32 Disconnected by Quitting'),
(9066, 1, '2017-10-25 09:23:04 Disconnected by Quitting'),
(9067, 1, '2017-10-25 09:37:08 Disconnected by Quitting'),
(9068, 1, '2017-10-25 09:46:45 Disconnected by Quitting'),
(9069, 1, '2017-10-25 10:00:00 Disconnected by Quitting'),
(9070, 1, '2017-10-25 10:04:26 Disconnected by Quitting'),
(9071, 1, '2017-10-25 10:06:02 Disconnected by Quitting'),
(9072, 1, '2017-10-25 10:10:20 Disconnected by Quitting'),
(9073, 1, '2017-10-25 10:16:23 Disconnected by Quitting'),
(9074, 1, '2017-10-25 10:18:45 Disconnected by Quitting'),
(9075, 1, '2017-10-25 10:41:05 Disconnected by Quitting'),
(9076, 610, '2017-10-25 18:08:44 Disconnected by Quitting');

-- --------------------------------------------------------

--
-- Table structure for table `player_notes`
--

CREATE TABLE IF NOT EXISTS `player_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playersqlid` int(11) NOT NULL,
  `slotid` int(11) NOT NULL,
  `details` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `player_weapons`
--

CREATE TABLE IF NOT EXISTS `player_weapons` (
  `player_dbid` int(11) NOT NULL,
  `weapon_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ammo` int(10) unsigned NOT NULL DEFAULT '0',
  `given_by` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `player_dbid_2` (`player_dbid`,`weapon_id`),
  KEY `player_dbid` (`player_dbid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `OwnerSQL` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL,
  `ComplexID` int(11) NOT NULL,
  `ExteriorX` float NOT NULL,
  `ExteriorY` float NOT NULL,
  `ExteriorZ` float NOT NULL,
  `ExteriorID` int(11) NOT NULL,
  `ExteriorWorld` int(11) NOT NULL,
  `InteriorX` float NOT NULL,
  `InteriorY` float NOT NULL,
  `InteriorZ` float NOT NULL,
  `InteriorA` float NOT NULL,
  `InteriorID` int(11) NOT NULL,
  `InteriorWorld` int(11) NOT NULL,
  `MarketPrice` int(11) NOT NULL DEFAULT '15000',
  `Rentable` int(11) NOT NULL DEFAULT '0',
  `RentPrice` int(11) NOT NULL DEFAULT '1',
  `Locked` int(11) NOT NULL DEFAULT '0',
  `Faction` int(11) NOT NULL DEFAULT '0',
  `Money` int(11) NOT NULL DEFAULT '0',
  `Level` int(11) NOT NULL DEFAULT '1',
  `CheckPosX` float NOT NULL,
  `CheckPosY` float NOT NULL,
  `CheckPosZ` float NOT NULL,
  `Weapons0` int(11) NOT NULL DEFAULT '0',
  `Weapons1` int(11) NOT NULL DEFAULT '0',
  `Weapons2` int(11) NOT NULL DEFAULT '0',
  `Weapons3` int(11) NOT NULL DEFAULT '0',
  `Weapons4` int(11) NOT NULL DEFAULT '0',
  `Weapons5` int(11) NOT NULL DEFAULT '0',
  `Weapons6` int(11) NOT NULL DEFAULT '0',
  `Weapons7` int(11) NOT NULL DEFAULT '0',
  `Weapons8` int(11) NOT NULL DEFAULT '0',
  `Weapons9` int(11) NOT NULL DEFAULT '0',
  `Weapons10` int(11) NOT NULL DEFAULT '0',
  `Weapons11` int(11) NOT NULL DEFAULT '0',
  `Weapons12` int(11) NOT NULL DEFAULT '0',
  `Weapons13` int(11) NOT NULL DEFAULT '0',
  `Weapons14` int(11) NOT NULL DEFAULT '0',
  `Weapons15` int(11) NOT NULL DEFAULT '0',
  `Weapons16` int(11) NOT NULL DEFAULT '0',
  `Weapons17` int(11) NOT NULL DEFAULT '0',
  `Weapons18` int(11) NOT NULL DEFAULT '0',
  `Weapons19` int(11) NOT NULL DEFAULT '0',
  `Weapons20` int(11) NOT NULL DEFAULT '0',
  `Ammo0` int(11) NOT NULL DEFAULT '0',
  `Ammo1` int(11) NOT NULL DEFAULT '0',
  `Ammo2` int(11) NOT NULL DEFAULT '0',
  `Ammo3` int(11) NOT NULL DEFAULT '0',
  `Ammo4` int(11) NOT NULL DEFAULT '0',
  `Ammo5` int(11) NOT NULL DEFAULT '0',
  `Ammo6` int(11) NOT NULL DEFAULT '0',
  `Ammo7` int(11) NOT NULL DEFAULT '0',
  `Ammo8` int(11) NOT NULL DEFAULT '0',
  `Ammo9` int(11) NOT NULL DEFAULT '0',
  `Ammo10` int(11) NOT NULL DEFAULT '0',
  `Ammo11` int(11) NOT NULL DEFAULT '0',
  `Ammo12` int(11) NOT NULL DEFAULT '0',
  `Ammo13` int(11) NOT NULL DEFAULT '0',
  `Ammo14` int(11) NOT NULL DEFAULT '0',
  `Ammo15` int(11) NOT NULL DEFAULT '0',
  `Ammo16` int(11) NOT NULL DEFAULT '0',
  `Ammo17` int(11) NOT NULL DEFAULT '0',
  `Ammo18` int(11) NOT NULL DEFAULT '0',
  `Ammo19` int(11) NOT NULL DEFAULT '0',
  `Ammo20` int(11) NOT NULL DEFAULT '0',
  `HasXMR` int(11) NOT NULL DEFAULT '0',
  `XMRPosX` float NOT NULL,
  `XMRPosY` float NOT NULL,
  `XMRPosZ` float NOT NULL,
  `XMRRotX` float NOT NULL,
  `XMRRotY` float NOT NULL,
  `XMRRotZ` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=611 ;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `OwnerSQL`, `Type`, `ComplexID`, `ExteriorX`, `ExteriorY`, `ExteriorZ`, `ExteriorID`, `ExteriorWorld`, `InteriorX`, `InteriorY`, `InteriorZ`, `InteriorA`, `InteriorID`, `InteriorWorld`, `MarketPrice`, `Rentable`, `RentPrice`, `Locked`, `Faction`, `Money`, `Level`, `CheckPosX`, `CheckPosY`, `CheckPosZ`, `Weapons0`, `Weapons1`, `Weapons2`, `Weapons3`, `Weapons4`, `Weapons5`, `Weapons6`, `Weapons7`, `Weapons8`, `Weapons9`, `Weapons10`, `Weapons11`, `Weapons12`, `Weapons13`, `Weapons14`, `Weapons15`, `Weapons16`, `Weapons17`, `Weapons18`, `Weapons19`, `Weapons20`, `Ammo0`, `Ammo1`, `Ammo2`, `Ammo3`, `Ammo4`, `Ammo5`, `Ammo6`, `Ammo7`, `Ammo8`, `Ammo9`, `Ammo10`, `Ammo11`, `Ammo12`, `Ammo13`, `Ammo14`, `Ammo15`, `Ammo16`, `Ammo17`, `Ammo18`, `Ammo19`, `Ammo20`, `HasXMR`, `XMRPosX`, `XMRPosY`, `XMRPosZ`, `XMRRotX`, `XMRRotY`, `XMRRotZ`) VALUES
(610, 604, 3, 0, 2015.76, -1641.71, 13.782, 0, 0, -2158.81, 642.833, 1052.38, 174.53, 1, 59589, 1, 0, 1, 0, 0, 0, 1, -2167.14, 643.192, 1052.38, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `property_drugs`
--

CREATE TABLE IF NOT EXISTS `property_drugs` (
  `drug_id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `drug_quantity` int(11) NOT NULL DEFAULT '0',
  `drug_type` int(11) NOT NULL,
  PRIMARY KEY (`drug_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `property_drugs`
--

INSERT INTO `property_drugs` (`drug_id`, `property_id`, `slot_id`, `drug_quantity`, `drug_type`) VALUES
(1, 610, 1, 20, 4),
(2, 610, 2, 50, 1),
(4, 610, 4, 10, 4),
(5, 610, 3, 20, 4);

-- --------------------------------------------------------

--
-- Table structure for table `property_garages`
--

CREATE TABLE IF NOT EXISTS `property_garages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `PosA` float NOT NULL,
  `IntX` float NOT NULL,
  `IntY` float NOT NULL,
  `IntZ` float NOT NULL,
  `IntA` float NOT NULL,
  `InteriorID` int(11) NOT NULL,
  `InteriorWorld` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `Locked` int(11) NOT NULL DEFAULT '0',
  `Faction` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `property_garages`
--

INSERT INTO `property_garages` (`id`, `PosX`, `PosY`, `PosZ`, `PosA`, `IntX`, `IntY`, `IntZ`, `IntA`, `InteriorID`, `InteriorWorld`, `Type`, `PropertyID`, `Locked`, `Faction`) VALUES
(105, 2026.14, -1648.93, 13.555, 0, 609.108, -1.306, 1000.85, 0, 1, 95852, 4, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rentals`
--

CREATE TABLE IF NOT EXISTS `rentals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` int(11) NOT NULL,
  `color1` int(11) NOT NULL,
  `color2` int(11) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `PosA` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `rentals`
--

INSERT INTO `rentals` (`id`, `model`, `color1`, `color2`, `PosX`, `PosY`, `PosZ`, `PosA`) VALUES
(1, 560, 100, 22, 1592.97, -1727.06, 13.166, 270.801);

-- --------------------------------------------------------

--
-- Table structure for table `tolls`
--

CREATE TABLE IF NOT EXISTS `tolls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PointAX` float NOT NULL,
  `PointAY` float NOT NULL,
  `PointAZ` float NOT NULL,
  `PointBX` float NOT NULL,
  `PointBY` float NOT NULL,
  `PointBZ` float NOT NULL,
  `Price` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tolls`
--

INSERT INTO `tolls` (`id`, `PointAX`, `PointAY`, `PointAZ`, `PointBX`, `PointBY`, `PointBZ`, `Price`) VALUES
(1, 1821.39, -1849.73, 13.414, 1821.78, -1862.96, 13.414, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_fines`
--

CREATE TABLE IF NOT EXISTS `vehicle_fines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_dbid` int(11) NOT NULL,
  `issuer_name` varchar(34) NOT NULL,
  `fine_amount` int(11) NOT NULL,
  `fine_reason` varchar(128) NOT NULL,
  `fine_date` varchar(90) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_dbid` (`vehicle_dbid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_logs`
--

CREATE TABLE IF NOT EXISTS `vehicle_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_dbid` int(11) NOT NULL,
  `log_detail` varchar(128) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23298 ;

--
-- Dumping data for table `vehicle_logs`
--

INSERT INTO `vehicle_logs` (`log_id`, `vehicle_dbid`, `log_detail`) VALUES
(23200, 218, '2017-10-20 06:48:12 Player Frank_Herd entered seat 0'),
(23201, 218, '2017-10-20 06:48:43 Frank_Herd put a AK47[100] (slot 1)'),
(23202, 218, '2017-10-20 06:49:15 Player Frank_Herd entered seat 0'),
(23203, 218, '2017-10-20 06:49:16 Frank_Herd took a AK47[100] (slot 1)'),
(23204, 218, '2017-10-20 07:18:10 Spawned'),
(23205, 218, '2017-10-20 07:18:26 Player Frank_Herd entered seat 0'),
(23206, 218, '2017-10-20 07:18:48 Frank_Herd put a AK47[100] (slot 1)'),
(23207, 218, '2017-10-20 07:18:55 Frank_Herd took a AK47[100] (slot 1)'),
(23208, 218, '2017-10-20 07:18:56 Parked by Frank_Herd'),
(23209, 218, '2017-10-20 07:21:56 Spawned'),
(23210, 218, '2017-10-20 07:22:03 Player Frank_Herd entered seat 0'),
(23211, 218, '2017-10-20 07:22:12 Frank_Herd put a AK47[62] (slot 1)'),
(23212, 218, '2017-10-20 07:25:58 Player Frank_Herd entered seat 0'),
(23213, 218, '2017-10-20 07:25:59 Frank_Herd took a AK47[62] (slot 1)'),
(23214, 218, '2017-10-20 07:25:59 Parked by Frank_Herd'),
(23215, 218, '2017-10-20 08:02:19 Spawned'),
(23216, 218, '2017-10-20 08:02:35 Player Frank_Herd entered seat 0'),
(23217, 218, '2017-10-20 08:02:44 Frank_Herd put a AK47[34] (slot 1)'),
(23218, 218, '2017-10-20 21:05:53 Spawned'),
(23219, 218, '2017-10-20 21:06:00 Player Frank_Herd entered seat 0'),
(23220, 218, '2017-10-20 21:12:38 Player Frank_Herd entered seat 0'),
(23221, 218, '2017-10-20 21:15:04 Player Frank_Herd entered seat 0'),
(23222, 218, '2017-10-20 21:31:36 Spawned'),
(23223, 218, '2017-10-20 21:31:42 Player Frank_Herd entered seat 0'),
(23224, 219, '2017-10-21 07:46:47 Player Frank_Herd entered seat 0'),
(23225, 219, '2017-10-21 07:46:49 Parked by Frank_Herd'),
(23226, 220, '2017-10-21 07:51:35 Player Frank_Herd entered seat 0'),
(23227, 220, '2017-10-21 07:51:37 Parked by Frank_Herd'),
(23228, 221, '2017-10-21 08:10:54 Player Frank_Herd entered seat 0'),
(23229, 221, '2017-10-21 08:10:56 Parked by Frank_Herd'),
(23230, 221, '2017-10-21 08:27:39 Spawned'),
(23231, 221, '2017-10-21 08:27:54 Player Frank_Herd entered seat 0'),
(23232, 220, '2017-10-21 08:27:59 Spawned'),
(23233, 220, '2017-10-21 08:28:03 Player Frank_Herd entered seat 0'),
(23234, 219, '2017-10-21 08:28:06 Spawned'),
(23235, 219, '2017-10-21 08:28:08 Player Frank_Herd entered seat 0'),
(23236, 222, '2017-10-21 13:13:45 Player Frank_Herd entered seat 0'),
(23237, 222, '2017-10-21 13:13:47 Parked by Frank_Herd'),
(23238, 223, '2017-10-21 13:14:40 Player Frank_Herd entered seat 0'),
(23239, 223, '2017-10-21 13:14:42 Parked by Frank_Herd'),
(23240, 224, '2017-10-21 13:15:43 Player Frank_Herd entered seat 0'),
(23241, 224, '2017-10-21 13:15:44 Parked by Frank_Herd'),
(23242, 225, '2017-10-21 13:16:26 Player Frank_Herd entered seat 0'),
(23243, 225, '2017-10-21 13:16:27 Parked by Frank_Herd'),
(23244, 226, '2017-10-21 13:17:13 Player Frank_Herd entered seat 0'),
(23245, 226, '2017-10-21 13:17:15 Parked by Frank_Herd'),
(23246, 227, '2017-10-21 13:19:07 Player Frank_Herd entered seat 0'),
(23247, 227, '2017-10-21 13:19:10 Parked by Frank_Herd'),
(23248, 227, '2017-10-21 23:58:27 Spawned'),
(23249, 227, '2017-10-21 23:58:37 Player Frank_Herd entered seat 0'),
(23250, 227, '2017-10-21 23:58:38 Parked by Frank_Herd'),
(23251, 225, '2017-10-22 00:06:44 Spawned'),
(23252, 226, '2017-10-22 00:07:22 Spawned'),
(23253, 225, '2017-10-22 00:14:06 Parked by Frank_Herd'),
(23254, 226, '2017-10-22 00:14:07 Parked by Frank_Herd'),
(23255, 228, '2017-10-22 00:14:27 Player Frank_Herd entered seat 0'),
(23256, 228, '2017-10-22 00:14:28 Parked by Frank_Herd'),
(23257, 228, '2017-10-22 00:16:38 Spawned'),
(23258, 228, '2017-10-22 00:16:51 Player Frank_Herd entered seat 0'),
(23259, 228, '2017-10-22 00:16:55 Parked by Frank_Herd'),
(23260, 223, '2017-10-22 00:22:17 Spawned'),
(23261, 218, '2017-10-22 00:29:17 Spawned'),
(23262, 218, '2017-10-22 00:29:23 Parked by Frank_Herd'),
(23263, 228, '2017-10-22 00:44:39 Spawned'),
(23264, 228, '2017-10-22 00:44:47 Player Frank_Herd entered seat 0'),
(23265, 228, '2017-10-22 00:44:49 Parked by Frank_Herd'),
(23266, 227, '2017-10-22 00:49:36 Spawned'),
(23267, 227, '2017-10-22 00:49:43 Player Frank_Herd entered seat 0'),
(23268, 227, '2017-10-22 00:49:44 Parked by Frank_Herd'),
(23269, 229, '2017-10-22 00:50:33 Player Frank_Herd entered seat 0'),
(23270, 229, '2017-10-22 00:50:35 Parked by Frank_Herd'),
(23271, 229, '2017-10-22 00:50:55 Spawned'),
(23272, 229, '2017-10-22 00:51:34 Parked by Frank_Herd'),
(23273, 223, '2017-10-22 01:05:16 Spawned'),
(23274, 223, '2017-10-22 01:05:26 Parked by Frank_Herd'),
(23275, 218, '2017-10-22 02:30:05 Spawned'),
(23276, 218, '2017-10-22 02:30:14 Player Frank_Herd entered seat 0'),
(23277, 222, '2017-10-22 02:38:30 Spawned'),
(23278, 222, '2017-10-22 02:38:40 Player Frank_Herd entered seat 0'),
(23279, 223, '2017-10-22 02:39:30 Spawned'),
(23280, 223, '2017-10-22 02:39:38 Player Frank_Herd entered seat 0'),
(23281, 224, '2017-10-22 02:40:00 Spawned'),
(23282, 224, '2017-10-22 02:40:06 Player Frank_Herd entered seat 0'),
(23283, 225, '2017-10-22 02:44:22 Spawned'),
(23284, 225, '2017-10-22 02:44:30 Player Frank_Herd entered seat 0'),
(23285, 229, '2017-10-23 03:37:42 Spawned'),
(23286, 229, '2017-10-23 03:39:39 Player Frank_Herd entered seat 0'),
(23287, 229, '2017-10-23 03:39:40 Parked by Frank_Herd'),
(23288, 229, '2017-10-23 04:12:37 Spawned'),
(23289, 229, '2017-10-23 04:12:50 Richard_Williamson locked'),
(23290, 229, '2017-10-23 04:12:51 Richard_Williamson unlocked'),
(23291, 229, '2017-10-23 04:12:52 Player Richard_Williamson entered seat 0'),
(23292, 229, '2017-10-23 04:12:57 Richard_Williamson locked'),
(23293, 229, '2017-10-23 04:13:37 Richard_Williamson unlocked'),
(23294, 229, '2017-10-23 04:13:40 Richard_Williamson locked'),
(23295, 229, '2017-10-23 04:13:44 Frank_Herd unlocked'),
(23296, 229, '2017-10-23 04:13:45 Player Frank_Herd entered seat 0'),
(23297, 229, '2017-10-23 04:13:49 Parked by Frank_Herd');

-- --------------------------------------------------------

--
-- Table structure for table `weapon_attachments`
--

CREATE TABLE IF NOT EXISTS `weapon_attachments` (
  `playerdbid` int(11) NOT NULL,
  `weaponid` int(11) NOT NULL,
  `boneid` int(11) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  `PosZ` float NOT NULL,
  `RotX` float NOT NULL,
  `RotY` float NOT NULL,
  `RotZ` float NOT NULL,
  `Hidden` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `playerdbid` (`playerdbid`,`weaponid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weapon_attachments`
--

INSERT INTO `weapon_attachments` (`playerdbid`, `weaponid`, `boneid`, `PosX`, `PosY`, `PosZ`, `RotX`, `RotY`, `RotZ`, `Hidden`) VALUES
(604, 3, 1, -0.056, 0.189, 0.054, 0, 44.5, 0, 1),
(604, 23, 1, -0.056, 0.189, 0.054, 0, 44.5, 0, 1),
(604, 24, 1, -0.056, 0.189, 0.054, 0, 44.5, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `xmr_data`
--

CREATE TABLE IF NOT EXISTS `xmr_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xmr_name` varchar(90) NOT NULL,
  `xmr_url` varchar(128) NOT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `xmr_sub`
--

CREATE TABLE IF NOT EXISTS `xmr_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(90) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `player_fines`
--
ALTER TABLE `player_fines`
  ADD CONSTRAINT `player_fines_ibfk_1` FOREIGN KEY (`player_dbid`) REFERENCES `players` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `player_weapons`
--
ALTER TABLE `player_weapons`
  ADD CONSTRAINT `player_weapons_ibfk_1` FOREIGN KEY (`player_dbid`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `vehicle_fines`
--
ALTER TABLE `vehicle_fines`
  ADD CONSTRAINT `vehicle_fines_ibfk_1` FOREIGN KEY (`vehicle_dbid`) REFERENCES `ownedcars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
